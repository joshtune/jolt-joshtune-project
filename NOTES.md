# NOTES
Just want to thank JayneAnn Stewart and Jason Bowerman for taking the time 
to talk with me.  

As for my technical experience, my [resume](JOSH-TUNE-RESUME.pdf) which I have 
attached in the source code submitted will show you where I have been for the 
past 10+ years and one things you will notice, is that, I have always been in 
the field of Software.  

I spent most of my early years in my career working as a Systems and Hardware 
Administrator which quickly peeked my interests in Software.  I started with 
Content Management Systems which lead to Software Development.  Most of my 
recent experience has been around FrontEnd (Angular, React, Knockout), Automation 
(Jenkins, Kubernetes, Teamcity), Containerization (Docker, Kubernetes) and some 
Ruby work, but that has not limit my scope of work.  I am a freelancer on the 
side too, where I work on systems integrations that utilize classic SAOP APIs,
payment vendors, AWS Lambda, node, php, laravel, which from what I hear through 
JayneAnn, Jolt is in the process of migrating out of PHP towards node.  

I would love to sit with you guys to further discuss my experiences and find out 
more about Jolt when possible.  JayneAnne says, there is hardware development 
also in-house, which sound interesting.

I am confident, my experience will bring value to your customers and team.  
Please feel free to reach out to me either through email (joshua.tune@gmail.com) 
or phone (801-860-1598).

I created a [README.md](./README.md) page that should have all my thoughts 
and notes regarding my experience with this project.
