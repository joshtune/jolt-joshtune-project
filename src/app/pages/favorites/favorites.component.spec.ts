import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { FavoritesComponent } from './favorites.component';
import {RouterModule} from '@angular/router';
import {LocalStorageService} from '../../shared/services/local-storage.service';
import {FavoritesModule} from './favorites.module';
import {APP_BASE_HREF} from '@angular/common';

describe('FavoritesComponent', () => {
  let component: FavoritesComponent;
  let fixture: ComponentFixture<FavoritesComponent>;
  let localStorageService: LocalStorageService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([]),
        FavoritesModule
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesComponent);
    localStorageService = TestBed.get(LocalStorageService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onFavoriteChange()', () => {
    it('should create', fakeAsync(() => {
      spyOn(component, 'loadData').and.callThrough();
      component.onFavoriteChange();
      tick(800);
      fixture.detectChanges();
      expect(component.loadData).toHaveBeenCalled();
    }));
  });
});
