import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '../../shared/services/local-storage.service';
import {SwapiPerson} from '../../shared/services/swapi.types';

@Component({
  selector: 'jjp-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  data: SwapiPerson[] = [];
  likeCount: number;
  loading = true;

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.loading = true;
    this.loadData();
  }

  loadData(): void {
    this.data = this.localStorageService.get('favorite.people');
    this.likeCount = this.data && this.data.length ? this.data.length : 0;
    this.loading = false;
  }

  onFavoriteChange(): void {
    this.loading = true;
    setTimeout(() => this.loadData(), 800);
  }

}
