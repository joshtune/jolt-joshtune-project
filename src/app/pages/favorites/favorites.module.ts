import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesComponent } from './favorites.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {PersonModule} from '../../features/person/person.module';

const routes: Routes = [
  {
    path: '',
    component: FavoritesComponent,
  }
];

@NgModule({
  declarations: [FavoritesComponent],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    PersonModule
  ]
})
export class FavoritesModule { }
