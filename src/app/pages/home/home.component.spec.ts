import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {GetPeopleResult, SwapiService} from '../../shared/services/swapi.service';
import {of, throwError} from 'rxjs';
import {SwapiPerson} from '../../shared/services/swapi.types';
import {HomeModule} from './home.module';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let swapiService: SwapiService;

  const mockGetPeopleResult = {
    count: 50,
    next: 'NEXT_STR',
    previous: 'PREVIOUS_STR',
    results: [],
  } as GetPeopleResult;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([]),
        HomeModule
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    swapiService = TestBed.get(SwapiService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should call swapiService.getPeople', () => {
      spyOn(swapiService, 'getPeople').and.callFake(() => {
        return of(mockGetPeopleResult);
      });
      component.ngOnInit();
      expect(swapiService.getPeople).toHaveBeenCalled();
    });

    it('should call swapiService.getPeople', () => {
      spyOn(swapiService, 'getPeople').and.callFake(() => {
        return throwError('MOCK_ERROR');
      });
      component.ngOnInit();
      expect(swapiService.getPeople).toHaveBeenCalled();
      expect(component.loading).toEqual(false);
    });

  });

  describe('ngOnDestroy()', () => {
  });

  describe('onSearchBoxChange()', () => {
    it('should call swapiService.getPeople', () => {
      spyOn(swapiService, 'queryPeople').and.callFake(() => {
        return of(mockGetPeopleResult);
      });
      component.onSearchBoxChange('test');
      expect(swapiService.queryPeople).toHaveBeenCalled();
    });

    it('should call swapiService.getPeople', () => {
      spyOn(swapiService, 'queryPeople').and.callFake(() => {
        return throwError('MOCK_ERROR');
      });
      component.onSearchBoxChange('test');
      expect(swapiService.queryPeople).toHaveBeenCalled();
    });

    it('should NOT call swapiService.getPeople if query is a number', () => {
      spyOn(swapiService, 'queryPeople').and.callFake(() => {
        return throwError('MOCK_ERROR');
      });
      component.onSearchBoxChange(234);
      expect(swapiService.queryPeople).not.toHaveBeenCalled();
    });

    it('should NOT call swapiService.getPeople if query is an object', () => {
      spyOn(swapiService, 'queryPeople').and.callFake(() => {
        return throwError('MOCK_ERROR');
      });
      component.onSearchBoxChange({name: 'test'});
      expect(swapiService.queryPeople).not.toHaveBeenCalled();
    });

    it('should NOT call swapiService.getPeople if query is an array', () => {
      spyOn(swapiService, 'queryPeople').and.callFake(() => {
        return throwError('MOCK_ERROR');
      });
      component.onSearchBoxChange(['1', 2, '333']);
      expect(swapiService.queryPeople).not.toHaveBeenCalled();
    });


  });

  describe('onPaginationChange()', () => {
    it('should call swapiService.getPeople', () => {
      spyOn(swapiService, 'getPeople').and.callFake(() => {
        return of(mockGetPeopleResult);
      });
      component.onPaginationChange(5);
      expect(swapiService.getPeople).toHaveBeenCalled();
    });

    it('should call swapiService.getPeople', () => {
      spyOn(swapiService, 'getPeople').and.callFake(() => {
        return throwError('MOCK_ERROR');
      });
      component.onPaginationChange(10);
      expect(swapiService.getPeople).toHaveBeenCalled();
      expect(component.loading).toEqual(false);
    });
  });

  describe('getPeopleHandler()', () => {
    it('should call swapiService.getPeople', () => {
      component.getPeopleHandler(mockGetPeopleResult);
      expect(component.totalRecordsCount).toEqual(50);
      expect(component.loading).toEqual(false);
    });
  });

  describe('onFavoriteChange()', () => {
    it('should increment component.likeCount', () => {
      component.likeCount = 5;
      component.onFavoriteChange({status: true, person: {} as SwapiPerson});
      expect(component.likeCount).toEqual(6);
      component.likeCount = 8;
      component.onFavoriteChange({status: true, person: {} as SwapiPerson});
      expect(component.likeCount).toEqual(9);
      component.likeCount = 55;
      component.onFavoriteChange({status: true, person: {} as SwapiPerson});
      expect(component.likeCount).toEqual(56);
    });

    it('should decrement component.likeCount', () => {
      component.likeCount = 5;
      component.onFavoriteChange({status: false, person: {} as SwapiPerson});
      expect(component.likeCount).toEqual(4);
      component.likeCount = 8;
      component.onFavoriteChange({status: false, person: {} as SwapiPerson});
      expect(component.likeCount).toEqual(7);
      component.likeCount = 55;
      component.onFavoriteChange({status: false, person: {} as SwapiPerson});
      expect(component.likeCount).toEqual(54);
    });
  });

});
