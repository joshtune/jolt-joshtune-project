import {Component, OnDestroy, OnInit} from '@angular/core';
import {SwapiPerson} from '../../shared/services/swapi.types';
import {
  GetPeopleResult,
  SwapiService,
} from '../../shared/services/swapi.service';
import {Subscription, throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {LocalStorageService} from '../../shared/services/local-storage.service';

@Component({
  selector: 'jjp-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  data: SwapiPerson[] = [];
  loading = true;
  likeCount: number;

  searchStr: string;

  totalRecordsCount = 0;

  constructor(
    private swapiService: SwapiService,
    private localStorageService: LocalStorageService
  ) {
  }

  ngOnInit() {
    // ToDo: Getting Favorite People would be better off abstracted to a service
    const favoritePeople = this.localStorageService.get('favorite.people');
    this.likeCount = (favoritePeople && favoritePeople.length ? favoritePeople.length : 0) as number;

    const sub = this.swapiService
      .getPeople()
      .subscribe(
        (response: GetPeopleResult) => this.getPeopleHandler(response),
        (error: HttpErrorResponse) => {
          this.loading = false;
          throwError(error);
        }
      );
    this.subscriptions.push(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  onSearchBoxChange(searchStr: any): void {
    const isString = !!(typeof searchStr === 'string' || searchStr instanceof String);
    if (this.searchStr !== searchStr && isString) {
      this.searchStr = searchStr;
      this.loading = true;
      const sub = this.swapiService.queryPeople(searchStr)
        .subscribe(
          (response: GetPeopleResult) => this.getPeopleHandler(response),
          (error: HttpErrorResponse) => {
            this.loading = false;
            throwError(error);
          }
        );
      this.subscriptions.push(sub);
    }
  }

  onPaginationChange(page: number) {
    this.loading = true;
    const sub = this.swapiService
      .getPeople(page)
      .subscribe(
        (response: GetPeopleResult) => this.getPeopleHandler(response),
        (error: HttpErrorResponse) => {
          this.loading = false;
          throwError(error);
        }
      );
    this.subscriptions.push(sub);
  }

  getPeopleHandler(response: GetPeopleResult): void {
    this.data = response && response.results ? response.results : [];
    this.totalRecordsCount = response && response.count ? response.count : 0;
    this.loading = false;
  }

  onFavoriteChange(update: { status: boolean, person: SwapiPerson }): void {
    if (update.status) {
      this.likeCount++;
    } else {
      this.likeCount--;
    }
  }
}
