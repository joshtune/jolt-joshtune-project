import {Component, Input} from '@angular/core';

@Component({
  selector: 'jjp-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  @Input() likeCount: number;
}
