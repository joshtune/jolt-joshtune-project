import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {PaginatorComponent} from './paginator.component';
import {APP_BASE_HREF} from '@angular/common';

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;
  let fixture: ComponentFixture<PaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaginatorComponent],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('gotToPage()', () => {
    it('should trigger change.emit', () => {
      spyOn(component.change, 'emit').and.callThrough();
      component.total = 30;
      component.increment = 5;
      component.totalPageCount = 6;
      component.goToPage(4);
      expect(component.change.emit).toHaveBeenCalledWith(4);
    });
    it('should trigger change.emit', () => {
      spyOn(component.change, 'emit').and.callThrough();
      component.total = 30;
      component.increment = 5;
      component.totalPageCount = 3;
      component.goToPage(1);
      expect(component.change.emit).toHaveBeenCalledWith(3);
    });
  });

  describe('next()', () => {
    it('should trigger component.goToPage appropriately', () => {
      component.currentPageNumber = 1;
      component.totalPageCount = 5;
      spyOn(component, 'goToPage').and.callThrough();
      component.next();
      expect(component.goToPage).toHaveBeenCalledWith(2);
    });
    it('should trigger component.goToPage appropriately', () => {
      component.currentPageNumber = 6;
      component.totalPageCount = 5;
      spyOn(component, 'goToPage').and.callThrough();
      component.next();
      expect(component.goToPage).toHaveBeenCalledWith(6);
    });
  });

  describe('previous()', () => {
    it('should trigger component.goToPage appropriately', () => {
      component.currentPageNumber = 5;
      spyOn(component, 'goToPage').and.callThrough();
      component.previous();
      expect(component.goToPage).toHaveBeenCalledWith(4);
    });
    it('should trigger component.goToPage appropriately', () => {
      component.currentPageNumber = 2;
      spyOn(component, 'goToPage').and.callThrough();
      component.previous();
      expect(component.goToPage).toHaveBeenCalledWith(1);
    });
  });

  describe('setTotalPages()', () => {
    it('should trigger component.goToPage appropriately', () => {
      component.total = 21;
      component.increment = 10;
      component.setTotalPages();
      expect(component.totalPageCount).toEqual(3);
    });
  });

});
