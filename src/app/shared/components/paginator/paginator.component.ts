import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'jjp-paginator',
  templateUrl: './paginator.component.html'
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() total: number;
  @Input() numberOfRecordsDisplayed: number;
  @Output() change = new EventEmitter();

  increment = 10;
  currentPageNumber = 1;
  totalPageCount = 0;

  ngOnInit(): void {
    this.setTotalPages();
  }

  goToPage(page: number): void {
    if (page <= this.totalPageCount && page >= 1) {
      if (page === 1) {
        this.currentPageNumber = this.totalPageCount;
      } else {
        this.currentPageNumber = page;
      }
    } else {
      this.currentPageNumber = 1;
    }
    this.change.emit(this.currentPageNumber);
  }

  next(): void {
    if (this.currentPageNumber <= this.totalPageCount) {
      this.currentPageNumber++;
    }
    this.goToPage(this.currentPageNumber);
  }

  previous(): void {
    if (this.currentPageNumber > 1) {
      this.currentPageNumber--;
    }
    this.goToPage(this.currentPageNumber);
  }

  setTotalPages(): void {
    const isThereExtra = !!(this.total % this.increment);
    this.totalPageCount = Math.floor(this.total / this.increment);
    if (isThereExtra) {
      this.totalPageCount++;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.total && changes.total.currentValue !== changes.total.previousValue) {
      this.total = changes.total.currentValue;
      this.setTotalPages();
    }
  }
}
