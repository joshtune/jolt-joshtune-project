import {TestBed} from '@angular/core/testing';

import {GetPeopleResult, SwapiService} from './swapi.service';
import {SharedModule} from '../shared.module';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';
import {SwapiPlanet} from './swapi.types';

describe('SwapiService', () => {
  let service: SwapiService;
  let httpMock: HttpTestingController;
  let mockPeopleResult: GetPeopleResult;
  let mockPlanet: SwapiPlanet;

  // use this as a guide https://medium.com/netscape/testing-with-the-angular-httpclient-api-648203820712

  beforeEach(() => TestBed.configureTestingModule({
    imports: [SharedModule, HttpClientTestingModule],
    providers: []
  }));

  beforeEach(() => {
    service = TestBed.get(SwapiService);
    httpMock = TestBed.get(HttpTestingController);
    mockPlanet = {
      name: 'test',
      rotation_period: 'string',
      orbital_period: 'string',
      diameter: 4,
      climate: 'string',
      gravity: 'string',
      terrain: 'string',
      surface_water: 'string',
      population: 'string',
      residents: ['test'],
      films: ['test'],
      created: 'string',
      edited: 'string',
      url: 'string',
    };
    mockPeopleResult = {
      count: 5,
      next: 'NEXT_STR',
      previous: 'PREVIOUS_STR',
      results: []
    } as GetPeopleResult;
  });


  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getPeople()', () => {
    it('should make a request to ${environment.swapiApiBaseUrl}/people/', () => {
      service.getPeople().subscribe((response: GetPeopleResult) => {
        expect(response.count).toEqual(5);
      });
      const req = httpMock.expectOne(`${environment.swapiApiBaseUrl}/people/`);
      expect(req.request.method).toBe('GET');
      req.flush(mockPeopleResult);
    });

    it('should make a request to ${environment.swapiApiBaseUrl}/people/?page=', () => {
      service.getPeople(1).subscribe((response: GetPeopleResult) => {
        expect(response.count).toEqual(5);
      });
      const req = httpMock.expectOne(`${environment.swapiApiBaseUrl}/people/?page=1`);
      expect(req.request.method).toBe('GET');
      req.flush(mockPeopleResult);
    });
  });

  describe('queryPeople()', () => {
    it('should make a request to ${environment.swapiApiBaseUrl}/people/?search=', () => {
      service.queryPeople('hello').subscribe((response: GetPeopleResult) => {
        expect(response.count).toEqual(5);
      });
      const req = httpMock.expectOne(`${environment.swapiApiBaseUrl}/people/?search=hello`);
      expect(req.request.method).toBe('GET');
      req.flush(mockPeopleResult);
    });
  });

  describe('getPlantById()', () => {
    it('should make a request to ${environment.swapiApiBaseUrl}/planets/{id}/', () => {
      service.getPlantById(5).subscribe((response: SwapiPlanet) => {
        expect(response).toBeTruthy();
      });
      const req = httpMock.expectOne(`${environment.swapiApiBaseUrl}/planets/5/`);
      expect(req.request.method).toBe('GET');
      req.flush(mockPlanet);
    });
  });

});
