import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { SwapiPerson, SwapiPlanet } from './swapi.types';

export interface GetPeopleResult {
  count: number;
  next: string;
  previous: string;
  results: SwapiPerson[];
}

@Injectable({
  providedIn: 'root',
})
export class SwapiService {
  baseUrl: string = environment.swapiApiBaseUrl;

  constructor(private http: HttpClient) {}

  getPeople(page?: number): Observable<GetPeopleResult> {
    let requestUrl;
    if (!page) {
      requestUrl = `${this.baseUrl}/people/`;
    } else {
      requestUrl = `${this.baseUrl}/people/?page=${page}`;
    }
    return this.http.get<GetPeopleResult>(requestUrl);
  }

  queryPeople(searchStr: string): Observable<GetPeopleResult> {
    // ToDo: Cache results in memory or local storage of
    //    search queries and cache them by searchStr as key
    const requestUrl = `${this.baseUrl}/people/?search=${searchStr}`;
    return this.http.get<GetPeopleResult>(requestUrl);
  }

  getPlantById(id: number): Observable<SwapiPlanet> {
    const requestUrl = `${this.baseUrl}/planets/${id}/`;
    return this.http.get<SwapiPlanet>(requestUrl);
  }
}
