import {TestBed} from '@angular/core/testing';
import {WindowRefService} from './window-ref.service';
import {APP_BASE_HREF} from '@angular/common';


describe('WindowRefService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      WindowRefService,
      {provide: APP_BASE_HREF, useValue: '/'}
    ]
  }));

  it('should be created', () => {
    const service: WindowRefService = TestBed.get(WindowRefService);
    expect(service).toBeTruthy();
  });
});
