import {Injectable} from '@angular/core';
import {WindowRefService} from './window-ref.service';

export interface MemoryItem {
  namespace: string;
  value: any;
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private windowRef: WindowRefService) {}

  set(key: string, val: any): void {
    this.windowRef.nativeWindow.localStorage.setItem(key, JSON.stringify(val));
  }

  get(key: string): any {
    const value = this.windowRef.nativeWindow.localStorage.getItem(key);
    return value && value.length ? JSON.parse(value) : undefined;
  }
}
