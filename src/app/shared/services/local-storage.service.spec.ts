import { TestBed } from '@angular/core/testing';
import { LocalStorageService } from './local-storage.service';

import {WindowRefService} from './window-ref.service';
import {APP_BASE_HREF} from '@angular/common';

describe('LocalStorageService', () => {
  let service: LocalStorageService;
  let windowRefService: WindowRefService;
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      WindowRefService,
      { provide: APP_BASE_HREF, useValue: '/' }
    ]
  }));
  beforeEach(() => {
    service = TestBed.get(LocalStorageService);
    windowRefService = TestBed.get(WindowRefService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('set()', () => {
    it('should call window.setItem', () => {
      spyOn(windowRefService.nativeWindow.localStorage, 'setItem').and.callFake(() => {});
      service.set('key', 'hello');
      expect(windowRefService.nativeWindow.localStorage.setItem).toHaveBeenCalledWith('key', '"hello"');
    });
  });

  describe('get()', () => {
    it('should call window.setItem', () => {
      spyOn(windowRefService.nativeWindow.localStorage, 'getItem').and.callFake(() => {
        return '"hey"';
      });
      service.get('key');
      expect(windowRefService.nativeWindow.localStorage.getItem).toHaveBeenCalledWith('key');
    });
  });
});
