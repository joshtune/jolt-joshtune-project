import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'swapiResourceId',
})
export class SwapiResourceIdPipe implements PipeTransform {
  transform(resourceUrl: string): number {
    const frag = resourceUrl.split('/');
    const lastEmpty = frag[frag.length - 1] === '';
    return parseInt(
      lastEmpty ? frag[frag.length - 2] : frag[frag.length - 1],
      10
    );
  }
}
