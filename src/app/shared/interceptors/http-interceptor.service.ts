import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpInterceptorService implements HttpInterceptor {
  // As documented by https://angular.io/guide/http#http-interceptors
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const request = req.clone({
      headers: req.headers.set('Content-Type', 'application/json'),
    });
    // Handling Request Response https://itnext.io/handle-http-responses-with-httpinterceptor-and-toastr-in-angular-3e056759cb16
    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {

          // console.log('request successful');
        }
      }),
      catchError((error: any) => {
        if (error instanceof HttpErrorResponse) {
          // ToDo: Add some kind of Toastr here or Global logging like bugsnag.com
          // console.log(error.message, error.name, error);
        }
        return of(error);
      })
    );
  }
}
