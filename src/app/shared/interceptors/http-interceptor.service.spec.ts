import {TestBed} from '@angular/core/testing';

import {HttpInterceptorService} from './http-interceptor.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {SwapiService} from '../services/swapi.service';
import {APP_BASE_HREF} from '@angular/common';

xdescribe('HttpInterceptorService', () => {

  // Use the following https://alligator.io/angular/testing-http-interceptors/

  let swapiService: SwapiService;
  let httpInterceptorService: HttpInterceptorService;
  let httpMock: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      SwapiService,
      { provide: APP_BASE_HREF, useValue: '/' },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: httpInterceptorService,
        multi: true,
      },
    ]
  }));

  beforeEach(() => {
    swapiService = TestBed.get(swapiService);
    httpMock = TestBed.get(httpMock);
    httpInterceptorService = TestBed.get(HttpInterceptorService);
  });

  it('should be created', () => {
    expect(httpInterceptorService).toBeTruthy();
  });

  // ToDo: Need work work on that
  xdescribe('intercept()', () => {
    it('should ...', () => {
      swapiService.getPeople(1).subscribe(response => {
        expect(response).toBeTruthy();
      });
      const httpRequest = httpMock.expectOne(`${swapiService.baseUrl}/people/`);
      expect(httpRequest.request.headers.get('Content-Type')).toBe('application/json')
    });
  });
});
