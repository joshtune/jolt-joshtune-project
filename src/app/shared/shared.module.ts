import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {SwapiService} from './services/swapi.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HttpInterceptorService} from './interceptors/http-interceptor.service';
import {SwapiResourceIdPipe} from './pipes/swapi-resource-id.pipe';
import {PaginatorComponent} from './components/paginator/paginator.component';
import {FormsModule} from '@angular/forms';
import {WindowRefService} from './services/window-ref.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import {RouterModule} from '@angular/router';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    SwapiResourceIdPipe,
    PaginatorComponent,
    HeaderComponent,
  ],
  providers: [
    SwapiResourceIdPipe,
    SwapiService,
    WindowRefService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
  ],
  imports: [
    CommonModule,
    NgbModule,
    HttpClientModule,
    RouterModule,
    DragDropModule
  ],
  exports: [
    // Core
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    DragDropModule,

    // In House
    PageNotFoundComponent,
    PaginatorComponent,
    SwapiResourceIdPipe,
    HeaderComponent,
  ],
})
export class SharedModule {
}
