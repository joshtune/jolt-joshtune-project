import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'jjp-person-search-box',
  template: `
		<input
			class="form-control mb-3"
			[(ngModel)]="searchBoxModel"
			type="text"
			placeholder="Search"
			name="searchBoxModel"
			id="searchBoxModels"
			(ngModelChange)="this.searchBoxModelUpdate.next($event)"
		>
  `,
  styleUrls: ['./person-search-box.component.scss']
})
export class PersonSearchBoxComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  searchBoxModel: string;
  searchBoxModelUpdate = new Subject<string>();
  @Output() change = new EventEmitter();

  ngOnInit(): void {
    // https://stackblitz.com/edit/angular-debounce-input-example
    const sub = this.searchBoxModelUpdate.pipe(
      debounceTime(500),
      distinctUntilChanged()
    )
      .subscribe((searchStr: string) => {
        this.change.emit(searchStr);
      });
    this.subscriptions.push(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }
}
