import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { PersonSearchBoxComponent } from './person-search-box.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {APP_BASE_HREF} from '@angular/common';

describe('PersonSearchBoxComponent', () => {
  let component: PersonSearchBoxComponent;
  let fixture: ComponentFixture<PersonSearchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSearchBoxComponent ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ],
      imports: [
        SharedModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // ToDo: Work in Progress
  xdescribe('ngOnInit()', () => {
    it('should call emit on searchBoxModel change', fakeAsync((done) => {
      spyOn(component.change, 'emit').and.callThrough();
      component.ngOnInit();
      component.searchBoxModel = 'hello';
      tick(500);
      expect(component.change.emit).toHaveBeenCalled();
    }));
  });
});
