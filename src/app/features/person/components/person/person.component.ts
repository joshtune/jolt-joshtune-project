import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SwapiPerson} from '../../../../shared/services/swapi.types';

@Component({
  selector: 'jjp-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss'],
})
export class PersonComponent {
  @Input() person: SwapiPerson;

  @Output() favoriteChange = new EventEmitter();

  onFavoriteChange(status: boolean): void {
    this.favoriteChange.emit(status);
  }
}
