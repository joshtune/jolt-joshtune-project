import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SwapiPerson} from '../../../../shared/services/swapi.types';

import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {LocalStorageService} from '../../../../shared/services/local-storage.service';

@Component({
  selector: 'jjp-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss'],
})
export class PersonListComponent {
  @Input() people: SwapiPerson[] = [];
  @Input() loading: boolean;
  @Input() sortable = false;
  @Output() favoriteChange = new EventEmitter();

  get peopleList(): string[] {
    return this.people.map((person: SwapiPerson) => person.name);
  }

  constructor(private localStorageService: LocalStorageService) {
  }

  onFavoriteChange(status: boolean, person: SwapiPerson): void {
    const update = {status, person};
    this.favoriteChange.emit(update);
  }

  drop(event: CdkDragDrop<SwapiPerson[]>) {
    const array = this.moveItemInArray(this.peopleList, event.previousIndex, event.currentIndex);
    const newPeopleList = [];
    array.forEach((name: string) => {
      const personIndex = this.people.findIndex((person: SwapiPerson) => person.name === name);
      newPeopleList.push(this.people[personIndex]);
    });
    this.people = newPeopleList;
    this.localStorageService.set('favorite.people', this.people);
  }

  // ToDo: Abstract this out to a separate service
  // inspired by node_modules/@angular/cdk/esm2015/drag-drop.js
  moveItemInArray(array: string[], fromIndex: number, toIndex: number) {
    const from = this.clamp$1(fromIndex, array.length - 1);
    const to = this.clamp$1(toIndex, array.length - 1);
    if (from === to) {
      return array;
    }
    const target = array[from];
    const delta = to < from ? -1 : 1;
    for (let i = from; i !== to; i += delta) {
      array[i] = array[i + delta];
    }
    array[to] = target;
    return array;
  }

  // ToDo: Abstract this out to a separate service
  // inspired by node_modules/@angular/cdk/esm2015/drag-drop.js
  clamp$1(value, max) {
    return Math.max(0, Math.min(max, value));
  }
}
