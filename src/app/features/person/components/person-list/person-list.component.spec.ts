import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PersonListComponent} from './person-list.component';
import {SwapiPerson} from '../../../../shared/services/swapi.types';
import {WindowRefService} from '../../../../shared/services/window-ref.service';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {LocalStorageService} from '../../../../shared/services/local-storage.service';
import {APP_BASE_HREF} from '@angular/common';

describe('PersonListComponent', () => {
  let component: PersonListComponent;
  let fixture: ComponentFixture<PersonListComponent>;
  let localStorageService: LocalStorageService;

  const mockPerson = {
    name: 'John Doe'
  } as SwapiPerson;

  let targetArray: string[] = [];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonListComponent],
      providers: [
        WindowRefService,
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonListComponent);
    localStorageService = TestBed.get(LocalStorageService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onFavoriteChange()', () => {
    it('should trigger component.favoriteChange.emit', () => {
      spyOn(component.favoriteChange, 'emit').and.callThrough();
      component.onFavoriteChange(true, mockPerson);
      expect(component.favoriteChange.emit).toHaveBeenCalledWith({status: true, person: mockPerson});

      component.onFavoriteChange(false, mockPerson);
      expect(component.favoriteChange.emit).toHaveBeenCalledWith({status: false, person: mockPerson});
    });
  });

  describe('drop()', () => {

    beforeEach(() => {
      targetArray = [];
      for (let i = 0; i <= 10; ++i) {
        targetArray.push(`Person${i}`);
      }

      component.people = [];
      targetArray.forEach((name: string) => {
        component.people.push({
          name
        } as SwapiPerson);
      });
    });
    it('should call localStorageService.set', () => {
      spyOn(component, 'moveItemInArray').and.callThrough();
      spyOn(localStorageService, 'set').and.callThrough();
      const event = {
        previousIndex: 1,
        currentIndex: 1
      } as CdkDragDrop<SwapiPerson[]>;
      component.drop(event);
      expect(component.moveItemInArray).toHaveBeenCalledWith(component.peopleList, 1, 1);
      expect(localStorageService.set).toHaveBeenCalledWith('favorite.people', component.people);
    });
  });

  describe('moveItemInArray()', () => {

    beforeEach(() => {
      targetArray = [];
      for (let i = 0; i <= 10; ++i) {
        targetArray.push(`Person${i}`);
      }
    });

    it('should move array item 1 to array item 1', () => {
      // targetArray.toString() 'Person0,Person1,Person2,Person3,Person4,Person5,Person6,Person7,Person8,Person9,Person10'
      const newArray = component.moveItemInArray(targetArray, 1, 1);
      expect(newArray.toString()).toBe('Person0,Person1,Person2,Person3,Person4,Person5,Person6,Person7,Person8,Person9,Person10');
    });

    it('should move array item 3 to array item 1', () => {
      // targetArray.toString() 'Person0,Person1,Person2,Person3,Person4,Person5,Person6,Person7,Person8,Person9,Person10'
      const newArray = component.moveItemInArray(targetArray, 3, 1);
      expect(newArray.toString()).toBe('Person0,Person3,Person1,Person2,Person4,Person5,Person6,Person7,Person8,Person9,Person10');
    });

    it('should move array item 3 to array item 0', () => {
      // targetArray.toString() 'Person0,Person1,Person2,Person3,Person4,Person5,Person6,Person7,Person8,Person9,Person10'
      const newArray = component.moveItemInArray(targetArray, 3, 0);
      expect(newArray.toString()).toBe('Person3,Person0,Person1,Person2,Person4,Person5,Person6,Person7,Person8,Person9,Person10');
    });
  });
});
