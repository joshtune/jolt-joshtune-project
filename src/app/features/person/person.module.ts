import { NgModule } from '@angular/core';
import { PersonComponent } from './components/person/person.component';
import { PersonListComponent } from './components/person-list/person-list.component';
import { SharedModule } from '../../shared/shared.module';
import { PlanetsModule } from '../planets/planets.module';
import { PersonSearchBoxComponent } from './components/person-search-box/person-search-box.component';
import {FavoriteModule} from '../favorite/favorite.module';

@NgModule({
  declarations: [PersonComponent, PersonListComponent, PersonSearchBoxComponent],
  exports: [PersonComponent, PersonListComponent, PersonSearchBoxComponent],
  imports: [SharedModule, PlanetsModule, FavoriteModule],
})
export class PersonModule {}
