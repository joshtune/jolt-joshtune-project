import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {SwapiPlanet} from '../../../../shared/services/swapi.types';
import {SwapiService} from '../../../../shared/services/swapi.service';
import {SwapiResourceIdPipe} from '../../../../shared/pipes/swapi-resource-id.pipe';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';

@Component({
  selector: 'jjp-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss'],
})
export class PlanetComponent implements OnChanges, OnDestroy {

  subscriptions: Subscription[] = [];
  planet: SwapiPlanet;
  @Input() planetResourceUrl: string;

  constructor(
    private swapiService: SwapiService,
    private swapiResourcePipe: SwapiResourceIdPipe
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.planetResourceUrl &&
      changes.planetResourceUrl.currentValue &&
      changes.planetResourceUrl.currentValue !==
      changes.planetResourceUrl.previousValue
    ) {
      const resourceUrl = changes.planetResourceUrl.currentValue;
      const planetId = this.swapiResourcePipe.transform(resourceUrl);
      if (planetId) {
        const sub = this.swapiService
          .getPlantById(planetId)
          .subscribe(
            (planet: SwapiPlanet) => {
              this.planet = planet;
            },
            (error: HttpErrorResponse) => {
              throw error;
            }
          );
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }
}
