import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PlanetComponent} from './planet.component';
import {SharedModule} from '../../../../shared/shared.module';
import {SwapiService} from '../../../../shared/services/swapi.service';
import {SwapiResourceIdPipe} from '../../../../shared/pipes/swapi-resource-id.pipe';
import {throwError} from 'rxjs';
import {APP_BASE_HREF} from '@angular/common';

describe('PlanetComponent', () => {
  let component: PlanetComponent;
  let fixture: ComponentFixture<PlanetComponent>;
  let swapiService: SwapiService;
  let swapiResourcePipe: SwapiResourceIdPipe;

  const fakePlanetResource = 'https://swapi.co/api/planets/1/';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanetComponent],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ],
      imports: [SharedModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetComponent);
    component = fixture.componentInstance;
    swapiService = TestBed.get(SwapiService);
    swapiResourcePipe = TestBed.get(SwapiResourceIdPipe);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnChanges', () => {

    it('should call swapiService.getPlantById', () => {
      spyOn(swapiService, 'getPlantById').and.callThrough();
      const change: any = {
        planetResourceUrl: {
          currentValue: fakePlanetResource,
          previousValue: ''
        }
      };
      component.ngOnChanges(change);
      expect(swapiService.getPlantById).toHaveBeenCalled();
    });

    // ToDo: Lets table this for now.
    xit('should call throw an error', () => {
      spyOn(swapiService, 'getPlantById').and.callFake(() => {
        return throwError('error');
      });
      const change: any = {
        planetResourceUrl: {
          currentValue: fakePlanetResource,
          previousValue: ''
        }
      };
      component.ngOnChanges(change);
      expect(swapiService.getPlantById).toHaveBeenCalled();
      expect(swapiService.getPlantById).toThrowError('error');
    });

    it('should not call swapiService.getPlantById when planetId is not set', () => {
      spyOn(swapiService, 'getPlantById').and.callThrough();
      spyOn(swapiResourcePipe, 'transform').and.returnValue(0);
      const change: any = {
        planetResourceUrl: {
          currentValue: fakePlanetResource,
          previousValue: ''
        }
      };
      component.ngOnChanges(change);
      expect(swapiService.getPlantById).not.toHaveBeenCalled();
    });
  });
});
