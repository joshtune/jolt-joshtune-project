import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetComponent } from './components/planet/planet.component';

@NgModule({
  declarations: [PlanetComponent],
  exports: [PlanetComponent],
  imports: [CommonModule],
})
export class PlanetsModule {}
