import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FavoriteButtonComponent} from './favorite-button.component';
import {FavoriteModule} from '../../favorite.module';
import {LocalStorageService, MemoryItem} from '../../../../shared/services/local-storage.service';
import {SwapiPerson} from '../../../../shared/services/swapi.types';
import {SharedModule} from '../../../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';

describe('FavoriteButtonComponent', () => {
  let component: FavoriteButtonComponent;
  let fixture: ComponentFixture<FavoriteButtonComponent>;
  let localStorageService: LocalStorageService;

  const mockStarWarCharacter1 = {
    name: 'John Doe'
  } as SwapiPerson;

  const mockStarWarCharacter2 = {
    name: 'Mickey Mouse'
  } as SwapiPerson;

  const mockMemoryItem = {
    namespace: 'test',
    value: [mockStarWarCharacter2, mockStarWarCharacter1]
  } as MemoryItem;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ],
      imports: [
        FavoriteModule,
        SharedModule,
        RouterModule.forRoot([])
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(FavoriteButtonComponent);
    localStorageService = TestBed.get(LocalStorageService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {

    it('should component.like to equal true', () => {
      spyOn(component, 'getFavoritePeople').and.returnValue([mockStarWarCharacter2, mockStarWarCharacter1]);
      component.person = mockStarWarCharacter1;
      component.ngOnInit();
      expect(component.like).toEqual(true);
      expect(component.getFavoritePeople).toHaveBeenCalled();
    });

    it('should component.like to equal false', () => {
      spyOn(component, 'getFavoritePeople').and.returnValue([mockStarWarCharacter2]);
      component.person = mockStarWarCharacter1;
      component.ngOnInit();
      expect(component.like).toEqual(false);
      expect(component.getFavoritePeople).toHaveBeenCalled();
    });
  });

  describe('toggleFavorite()', () => {
    beforeEach(() => {
      spyOn(component, 'ngOnInit').and.callFake(() => {});
      spyOn(component.change, 'emit').and.callFake(() => {});
      spyOn(localStorageService, 'set').and.callFake(() => {});
      component.person = mockStarWarCharacter1;
    });

    it('should add person to component.favoritePeople', () => {
      spyOn(component, 'getFavoritePeople').and.returnValue([mockStarWarCharacter2]);
      component.like = false;
      component.toggleFavorite();
      const itemIndex = component.favoritePeople.findIndex((item: SwapiPerson) => item === mockStarWarCharacter1);
      expect(itemIndex).toEqual(1);
      expect(component.like).toEqual(true);
    });

    it('should remove person to component.favoritePeople', () => {
      spyOn(component, 'getFavoritePeople').and.returnValue([mockStarWarCharacter2, mockStarWarCharacter1]);
      component.person = mockStarWarCharacter1;
      component.like = false;
      component.toggleFavorite();
      const itemIndex = component.favoritePeople.findIndex((item: SwapiPerson) => item.name === mockStarWarCharacter1.name);
      expect(itemIndex).toEqual(-1);
      expect(component.like).toEqual(false);
    });
  });
  describe('getFavoritePeople()', () => {
    it('should remove person to component.favoritePeople', () => {
      spyOn(localStorageService, 'get').and.returnValue(mockMemoryItem.value);
      component.namespace = 'test';
      const favoritePeople = component.getFavoritePeople();
      expect(localStorageService.get).toHaveBeenCalledWith(component.namespace);
      expect(favoritePeople).toEqual(mockMemoryItem.value);
    });
  });
});
