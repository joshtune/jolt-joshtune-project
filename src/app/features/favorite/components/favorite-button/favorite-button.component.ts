import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SwapiPerson} from '../../../../shared/services/swapi.types';
import {LocalStorageService} from '../../../../shared/services/local-storage.service';

@Component({
  selector: 'jjp-favorite-button',
  templateUrl: './favorite-button.component.html'
})
export class FavoriteButtonComponent implements OnInit {

  @Input() person: SwapiPerson;
  @Output() change = new EventEmitter();

  favoritePeople: SwapiPerson[];

  like = false;

  namespace = 'favorite.people';

  constructor(private localStorage: LocalStorageService) {
  }

  ngOnInit(): void {
    this.favoritePeople = this.getFavoritePeople();
    if (this.person && this.person.name && this.person.name.length) {
      if (this.favoritePeople && this.favoritePeople.length) {
        const search = this.favoritePeople.filter((swapiPerson: SwapiPerson) => swapiPerson.name === this.person.name);
        this.like = !!(search && search.length);
      } else {
        this.like = false;
        this.favoritePeople = [];
      }
    }
  }

  toggleFavorite(): void {
    this.favoritePeople = this.getFavoritePeople();
    const search = this.favoritePeople && this.favoritePeople.length
      ? this.favoritePeople.filter((swapiPerson: SwapiPerson) => swapiPerson.name === this.person.name)
      : undefined;

    this.like = !!(search && search.length);

    if (this.like) {
      // remove favorite
      this.favoritePeople = this.favoritePeople.filter((swapiPerson: SwapiPerson) => swapiPerson.name !== this.person.name);
      this.like = false;
    } else {
      // add favorite
      this.favoritePeople.push(this.person);
      this.like = true;
    }
    this.change.emit(this.like);
    this.localStorage.set(this.namespace, this.favoritePeople);
  }

  getFavoritePeople(): SwapiPerson[] {
    const memoryItemValue: SwapiPerson[] = this.localStorage.get(this.namespace);
    return memoryItemValue ? memoryItemValue : [];
  }
}
