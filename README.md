![Star Wars Project](docs-assets/star-wars.jpg)
# Star Wars Project
This was such a fun project. You should be able to find the latest copy of this repo [here](https://bitbucket.org/joshtune/jolt-joshtune-project/).

Getting started took some thought, and when I thought I was in my groove, `life` kicked in and `family`, `work` and `personal` matters took me off, but I kept at it and every time I worked on something there is always something else I want to show, so I tried my best to keep a log of those as I went throught his.  You could see details below in the **Ideas** section below

## Who Am I
```
Name: Joshua Tune
LinkedIn: https://www.linkedin.com/in/joshtune
```

## Code Coverage
Current code coverage base on this last commit is ...
```
HeadlessChrome 76.0.3809 (Mac OS X 10.14.6): Executed 53 of 57 (skipped 4) SUCCESS (0.716 secs / 0.562 secs)
TOTAL: 53 SUCCESS
TOTAL: 53 SUCCESS
TOTAL: 53 SUCCESS

=============================== Coverage summary ===============================
Statements   : 96.55% ( 224/232 )
Branches     : 82.35% ( 70/85 )
Functions    : 92.65% ( 63/68 )
Lines        : 96.43% ( 189/196 )
================================================================================
```
![Code Coverage](./docs-assets/code-coverage.png)

I have learn from my 10+ years career how important it is to have automation.  It keeps your business logic in tact, even the smallests tests might seem rediculous at times, but it is logic that you want to be aware of that when someone makes a change you should see it in the next PR review.

## Let's Get Started

This project is written in Angular 8, so before you get started, make sure you system meets the Prerequisite requirements as [outlined](https://angular.io/guide/setup-local), then run `npm install` in the root and you should be good to go.

The following should help you see what I have so far.

**Run the Project**
```
npm run app:start
```
Go to http://localhost/ and you should see something like

![Star Wars Project](docs-assets/heros.gif)

![Star Wars Project](docs-assets/favorites.gif)

**Run Unit Tests**
```
npm run app:test:headless
```

**Run Unit Tests Coverage**
```
npm run app:test:coverage
```
Then take a look at contents of `coverage` folder in your browser

**Run e2e Tests**
```
npm run app:e2e
```
I really thought I would be able to get to e2e tests, but due to time, I am not at this time.

## Ideas
As I worked on this project ideas kept coming to me and knowing I am might not be able to get to it, I have created a list as follows below. You should also note that throughout the code I used something like `// ToDo: ` to note ideas I would like to come back to later.

**Data Caching**

Currently when one fetches the data via the SwapiApiService.  It can sometimes take long time to load.  Caching response into Local Storage is an idea to speed that up.

**Likes Count Reset**

Resetting the likes is a pain when you have likes spread across several pages.  A reset all would be nice to have.

**Use the route as source of truth**

This is obviously a small project, in bigger projects where routes can easily be a rabbit hole, I have found it useful to use the route as a source truth for filters and pages.

**Docker**

WhereI currently work we work a lot with Docker, if I had more time I would like to put that in so one can run it locally in a docker container :)

**Bug Handling Toastr**

My http interceptor is setup to catch any unhandled errors which would be nice to expose in the UI using something like [toastr](https://ngx-toastr.netlify.com/)

## Known Bugs
I try to be perfect, it is not always possible :)

**Finicky Angular**

Not quite sure yet what is wrong, but the browser project does not seem to fully compile after you make changes to source files sometimes, so to fix right now is to just re-run `npm run app:start` which can get old quick.

**Offline View**

When there is no connection to the internet, the view does not look good

